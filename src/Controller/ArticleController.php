<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Service\SlackClient;
use Doctrine\ORM\EntityManagerInterface;
use Nexy\Slack\Client;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends AbstractController
{
	/**
	 * @var bool
	 */
	private $isDebug;

	/**
	 * ArticleController constructor.
	 *
	 * @param bool $isDebug
	 */
	public function __construct(bool $isDebug)
	{
		$this->isDebug = $isDebug;
	}

	/**
	 * @Route("/", name="app_homepage")
	 * @param ArticleRepository $repository
	 *
	 * @return Response
	 */
    public function homepage(ArticleRepository $repository)
    {
        $articles = $repository->findAllPublishedOrderedByNewest();

        return $this->render('article/homepage.html.twig', [
        	'articles' => $articles
        ]);
    }

	/**
	 * @Route("/news/{slug}", name="article_show")
	 * @param Article     $article
	 * @param SlackClient $slack
	 *
	 * @return Response
	 * @throws \Http\Client\Exception
	 */
    public function show(Article $article, SlackClient $slack) : Response
    {
    	if ($article->getSlug() === 'khaaaan') {
		    $slack->sendMessage('Khan', 'Ah, Kirk, my old friend...');
	    }

        return $this->render('article/show.html.twig', [
			'article' => $article,
        ]);
    }

    /**
     * @Route("/news/{slug}/heart", name="article_toggle_heart", methods={"POST"})
     */
    public function toggleArticleHeart(Article $article, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $article->incrementHeartCount();
	    $em->flush();
        $logger->info('Article is being hearted!');

        return new JsonResponse(['hearts' => $article->getHeartCount()]);
    }
}
