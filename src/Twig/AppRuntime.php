<?php


namespace App\Twig;

use App\Service\MarkdownHelper;
use Twig\Extension\RuntimeExtensionInterface;

/**
 * Class AppRuntime
 *
 * @package App\Twig
 */
class AppRuntime implements RuntimeExtensionInterface
{
	/**
	 * @var MarkdownHelper
	 */
	private $markdownHelper;

	/**
	 * AppRuntime constructor.
	 *
	 * @param MarkdownHelper $markdownHelper
	 */
	public function __construct(MarkdownHelper $markdownHelper)
	{
		$this->markdownHelper = $markdownHelper;
	}

	/**
	 * @param $value
	 *
	 * @return string
	 * @throws \Psr\Cache\InvalidArgumentException
	 */
	public function processMarkdown($value)
	{
		return $this->markdownHelper->parse($value);
	}
}