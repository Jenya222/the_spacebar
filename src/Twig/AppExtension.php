<?php

namespace App\Twig;

use App\Service\MarkdownHelper;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
	/**
	 * @var MarkdownHelper
	 */
	private $helper;

	/**
	 * AppExtension constructor.
	 *
	 * @param MarkdownHelper $helper
	 */
	public function __construct(MarkdownHelper $helper)
	{
		$this->helper = $helper;
	}

	/**
	 * @return array
	 */
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('cached_markdown', [AppRuntime::class, 'processMarkdown'], ['is_safe' => ['html']]),
        ];
    }
}
