<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Common\Persistence\ObjectManager;

class TagFixtures extends BaseFixtures
{
	/**
	 * @param ObjectManager $manager
	 *
	 * @return mixed
	 */
	protected function loadData(ObjectManager $manager)
	{
		$this->createMany(10,Tag::class, function() {
			$tag = new Tag();
			$tag->setName($this->faker->realText(20));

			return $tag;
		});

		$manager->flush();
	}
}
