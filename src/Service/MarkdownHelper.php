<?php


namespace App\Service;


use Michelf\MarkdownInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class MarkdownHelper
 *
 * @package App\Service
 */
class MarkdownHelper
{
	/**
	 * @var AdapterInterface
	 */
	public $cache;

	/**
	 * @var MarkdownInterface
	 */
	public $markdown;
	/**
	 * @var LoggerInterface
	 */
	private $logger;
	/**
	 * @var bool
	 */
	private $isDebug;

	/**
	 * @var Security
	 */
	private $security;

	/**
	 * MarkdownHelper constructor.
	 *
	 * @param MarkdownInterface $markdown
	 * @param AdapterInterface  $cache
	 * @param LoggerInterface   $markdownLogger
	 * @param bool              $isDebug
	 * @param Security          $security
	 */
	public function __construct(MarkdownInterface $markdown, AdapterInterface $cache, LoggerInterface $markdownLogger, bool $isDebug, Security $security)
	{
		$this->cache = $cache;
		$this->markdown = $markdown;
		$this->logger = $markdownLogger;
		$this->isDebug = $isDebug;
		$this->security = $security;
	}

	/**
	 * @param string $source
	 *
	 * @return string
	 * @throws \Psr\Cache\InvalidArgumentException
	 */
	public function parse(string $source) : string
	{
		if (stripos($source, 'bacon')) {
			$this->logger->info('They are talking about bacon again!', [
				'user' => $this->security->getUser(),
			]);
		}

		if ($this->isDebug) {
			return $this->markdown->transform($source);
		}

		$item = $this->cache->getItem('markdown_' . md5($source));

		if (!$item->isHit()) {
			$item->set($this->markdown->transform($source));
			$this->cache->save($item);
		}

		return $item->get();
	}
}