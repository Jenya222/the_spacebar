<?php


namespace App\Helper;


use Psr\Log\LoggerInterface;

/**
 * Trait LoggerTrait
 *
 * @package App\Helper
 */
trait LoggerTrait
{
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @required
	 * @param LoggerInterface $logger
	 */
	public function setLogger(LoggerInterface $logger): void
	{
		$this->logger = $logger;
	}

	/**
	 * @param string $message
	 * @param array  $context
	 */
	public function logInfo(string $message, array $context = []): void
	{
		if ($this->logger) {
			$this->logger->info($message, $context);
		}
	}
}